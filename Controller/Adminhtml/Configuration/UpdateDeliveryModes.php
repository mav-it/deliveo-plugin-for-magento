<?php

namespace Mavit\Deliveo\Controller\Adminhtml\Configuration;

use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Action\Action;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Result\Page;

class UpdateDeliveryModes extends Action
{
    public function execute()
    {
        $manager = \Magento\Framework\App\ObjectManager::getInstance();
        $obj = $manager->create('Mavit\Deliveo\Model\Config\Source\Delivery');

        $apiKey = null;
        $licence = null;
        if (array_key_exists('api_key', $this->getRequest()->getParams()) &&
            array_key_exists('licence', $this->getRequest()->getParams())) {
            $apiKey = $this->getRequest()->getParam('api_key');
            $licence = $this->getRequest()->getParam('licence');
        }

        $apiResponse = $obj->updateConfig($apiKey, $licence);
        if ($apiResponse == null) {
            die('error');
        }
        $options = json_decode($apiResponse, true);

        $html = '';

        $obj = $manager->create('Mavit\Deliveo\Model\Config\Source\CoreConfig');
        $defaultDeliveryMode = $obj->getConfig('deliveo/general/defaultdeliverymode');

        foreach ($options as $option) {
            $html .= '<option '.($defaultDeliveryMode==$option['value'] ? 'selected' : '').' value="'.$option['value'].'">'.$option['label'].'</option>';
        }

        die($html);
    }
}