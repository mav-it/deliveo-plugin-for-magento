<?php
namespace Mavit\Deliveo\Controller\Adminhtml\Configuration;

use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Action\Action;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Result\Page;

class Index extends Action
{
    private $configFormMap = [
        'deliveo/general/apikey' => 'api_key',
        'deliveo/general/license' => 'licence',
        'deliveo/general/sname' => 'sender',
        'deliveo/general/scountry' => 'sender_country',
        'deliveo/general/spostcode' => 'sender_zip',
        'deliveo/general/scity' => 'sender_city',
        'deliveo/general/saddress1' => 'sender_address',
        'deliveo/general/saddress2' => 'sender_apartment',
        'deliveo/general/phone' => 'sender_phone',
        'deliveo/general/email' => 'sender_email',
        'deliveo/general/width' => 'package_size_x',
        'deliveo/general/height' => 'package_size_y',
        'deliveo/general/depth' => 'package_size_z',
        'deliveo/general/weight' => 'defaultweight',
        'deliveo/general/currency' => 'webshopcurrency',
        'deliveo/general/correction_multiplier' => 'correction_multiplier',
        'deliveo/general/priority' => 'priority',
        'deliveo/general/saturday' => 'saturday',
        'deliveo/general/insurance' => 'insurance',
        'deliveo/general/orderref' => 'orderref',
        'deliveo/general/trackingid' => 'trackingid',
        'deliveo/general/freight' => 'freight',
        'deliveo/general/packaging_unit' => 'packaging_unit',
        'deliveo/general/defaultdeliverymode' => 'defaultdeliverymode',
        'deliveo/general/status' => 'set_status',
    ];

    public function execute()
    {
        if(count($this->getRequest()->getPostValue()) > 0) {
            $this->processPostForm();

            $redirect = $this->resultFactory->create(\Magento\Framework\Controller\ResultFactory::TYPE_REDIRECT);
            $redirect->setUrl($this->_redirect->getRefererUrl());
            return $redirect;
        }

        /** @var Page $page */
        $page = $this->resultFactory->create(ResultFactory::TYPE_PAGE);

        /** @var Template $block */
        $block = $page->getLayout()->getBlock('deliveo.config');

        $manager = \Magento\Framework\App\ObjectManager::getInstance();
        $obj = $manager->create('Magento\Sales\Model\ResourceModel\Order\Status\Collection');
        $block->setData('orderStatuses', $obj->toOptionArray());

        $obj = $manager->create('Mavit\Deliveo\Model\Config\Source\Delivery');
        $block->setData('deliveryOptions', $obj->toOptionArray());

        $block->setData('formConfig', $this->loadConfigToForm());

        return $page;
    }

    private function processPostForm()
    {
        $manager = \Magento\Framework\App\ObjectManager::getInstance();
        $coreConfig = $manager->create('Mavit\Deliveo\Model\Config\Source\CoreConfig');

        $configValues = [];
        foreach ($this->configFormMap as $configPath => $inputName) {
            $configValues[$configPath] = $this->getRequest()->getPostValue($inputName);
        }

        $configValues['deliveo/general/priority'] = $configValues['deliveo/general/priority'] == 'on' ? 1 : 0;
        $configValues['deliveo/general/saturday'] = $configValues['deliveo/general/saturday'] == 'on' ? 1 : 0;
        $configValues['deliveo/general/insurance'] = $configValues['deliveo/general/insurance'] == 'on' ? 1 : 0;
        $configValues['deliveo/general/orderref'] = $configValues['deliveo/general/orderref'] == 'on' ? 1 : 0;
        $configValues['deliveo/general/trackingid'] = $configValues['deliveo/general/trackingid'] == 'on' ? 1 : 0;

        foreach ($configValues as $configKey => $formValue) {
            $coreConfig->setConfig($configKey, $formValue);
        }

        $cache = $manager->create('\Magento\Framework\App\CacheInterface');
        $cache->clean(['config']);
    }

    private function loadConfigToForm() {
        $manager = \Magento\Framework\App\ObjectManager::getInstance();
        $coreConfig = $manager->create('Mavit\Deliveo\Model\Config\Source\CoreConfig');

        $formParams = [];
        foreach ($this->configFormMap as $configPath => $inputName) {
            $formParams[$inputName] = $coreConfig->getConfig($configPath);

            if (is_null($formParams[$inputName]) || $formParams[$inputName] === '') {
                switch ($inputName) {
                    case 'package_size_x':
                    case 'package_size_y':
                    case 'package_size_z':
                        $formParams[$inputName] = 10;
                        break;

                    case 'defaultweight':
                    case 'correction_multiplier':
                    case 'freight':
                        $formParams[$inputName] = 1;
                        break;

                    case 'packaging_unit':
                        $formParams[$inputName] = 3;
                        break;
                }
            }
        }

        return $formParams;
    }
}
