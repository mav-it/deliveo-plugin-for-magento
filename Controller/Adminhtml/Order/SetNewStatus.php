<?php

namespace Mavit\Deliveo\Controller\Adminhtml\Order;

use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Action\HttpPostActionInterface as HttpPostActionInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Magento\Sales\Api\OrderManagementInterface;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\ResourceModel\Order\CollectionFactory;
use Magento\Ui\Component\MassAction\Filter;
use Mavit\Deliveo\Model\Data;

class SetNewStatus extends \Magento\Sales\Controller\Adminhtml\Order\AbstractMassAction implements HttpPostActionInterface
{
    protected $request;
    /**
     * Authorization level of a basic admin session
     */
    const ADMIN_RESOURCE = 'Magento_Sales::deliveo';

    /**
     * @var OrderManagementInterface
     */
    private $orderManagement;

    public $packages;

    /**
     * @param Context $context
     * @param Filter $filter
     * @param CollectionFactory $collectionFactory
     * @param OrderManagementInterface|null $orderManagement
     * @var \Magento\Framework\App\RequestInterface
     */
    public function __construct(
        Context $context,
        Filter $filter,
        CollectionFactory $collectionFactory,
        OrderManagementInterface $orderManagement = null,
        Data $model,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    ) {
        $this->model             = $model;
        $this->scopeConfig       = $scopeConfig;
        $this->collectionFactory = $collectionFactory;
        $this->orderManagement   = $orderManagement ?: \Magento\Framework\App\ObjectManager::getInstance()->get(
            \Magento\Sales\Api\OrderManagementInterface::class
        );
        parent::__construct($context, $filter);
    }

    /**
     * Cancel selected orders
     *
     * @param AbstractCollection $collection
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    protected function massAction(AbstractCollection $collection)
    {
        foreach ($collection->getItems() as $order) {
            $packages[] = $this->Delive($order);
        }
        $resultRedirect = $this->resultRedirectFactory->create();
        return $resultRedirect->setPath($this->getComponentRefererUrl());
    }

    public $products;

    public function Delive($currentorder)
    {
        $order = $this->_objectManager->create('Magento\Sales\Model\Order')->load($currentorder->getEntityId());

        if (strlen($order->getDeliveoCode()) > 0) {
            return $this->messageManager->addErrorMessage(__('A csomagot már rögzítettük a Deliveo rendszerébe. Rendelésazonosító: ' . $order->getIncrementId() . ' csoportazonosító: ' . $order->getDeliveoCode(), 1));
        }

        $orderItems      = $order->getAllItems();
        $ShippingAddress = $order->getShippingAddress();
        $cod             = 0;

        if ($order->getPayment()->getMethodInstance() instanceof \Magento\OfflinePayments\Model\Cashondelivery) {
            $cod = $order->getTotalDue();
        }

        $products = array();
        foreach ($orderItems as $item) {
            if ($item->getParentItem()) {
                continue;
            }
            $products[] = array(
                'weight'  => intval($item->getWeight()),
                'x'       => intval($item->getWidth() ?? $this->scopeConfig->getValue('deliveo/general/width')),
                'y'       => intval($item->getHeight() ?? $this->scopeConfig->getValue('deliveo/general/height')),
                'z'       => intval($item->getLenght() ?? $this->scopeConfig->getValue('deliveo/general/dephth')),
                'item_no' => html_entity_decode($item->getSku()),
            );
        }

        $configPackagingUnit = $this->scopeConfig->getValue('deliveo/general/packaging_unit');
        $packagingUnit       = 1;
        if ($configPackagingUnit == '3') {
            $packagingUnit = $order->getDeliveoPackagingUnit() ?? 1;
        }
        if ($configPackagingUnit == '2') {
            $packagingUnit = count($products);
        }

        $delivery = $order->getDeliveoDeliveryMode() ?? $this->scopeConfig->getValue('deliveo/general/defaultdeliverymode');

        $post = [
            'delivery'          => $delivery,
            'sender'            => $this->scopeConfig->getValue('deliveo/general/sname'),
            'sender_zip'        => $this->scopeConfig->getValue('deliveo/general/spostcode'),
            'sender_city'       => $this->scopeConfig->getValue('deliveo/general/scity'),
            'sender_address'    => $this->scopeConfig->getValue('deliveo/general/saddress1'),
            'sender_country'    => $this->scopeConfig->getValue('deliveo/general/scountry'),
            'sender_apartment'  => $this->scopeConfig->getValue('deliveo/general/saddress2'),
            'sender_phone'      => $this->scopeConfig->getValue('deliveo/general/phone'),
            'sender_email'      => $this->scopeConfig->getValue('deliveo/general/email'),

            'consignee'         => $ShippingAddress->getFirstname() . " " . $ShippingAddress->getLastname(),
            'consignee_country' => $ShippingAddress->getCountryId(),
            'consignee_zip'     => $ShippingAddress->getPostcode(),
            'consignee_city'    => $ShippingAddress->getCity(),
            'consignee_address' => implode(", ", $ShippingAddress->getStreet()),
            'consignee_phone'   => $ShippingAddress->getTelephone(),
            'consignee_email'   => $ShippingAddress->getEmail(),

            'currency'          => $this->scopeConfig->getValue('deliveo/general/currency'),
            'comment'           => $order->getCustomerNote(),
            'packaging_unit'    => $packagingUnit,

            'optional_parameter_1'          => $this->scopeConfig->getValue('deliveo/general/priority'),
            'optional_parameter_3'          => $this->scopeConfig->getValue('deliveo/general/saturday'),
            'referenceid'       => $this->scopeConfig->getValue('deliveo/general/orderref') ? $order->getIncrementId() : '',
            'cod'               => $cod,
            'freight'           => $this->scopeConfig->getValue('deliveo/general/freight') == '1' ? "felado" : "cimzett",
            'optional_parameter_2'         => $this->scopeConfig->getValue('deliveo/general/insurance') ? $cod : 0,
            'tracking'          => $this->scopeConfig->getValue('deliveo/general/trackingid') ? $order->getIncrementId() : '',

            'packages'          => $products,
        ];

        $curl = curl_init();
        curl_setopt_array(
            $curl,
            array(
                CURLOPT_URL            => "https://api.deliveo.eu/package/create?licence=" .
                    $this->scopeConfig->getValue('deliveo/general/license') . "&api_key=" . $this->scopeConfig->getValue('deliveo/general/apikey'),
                CURLOPT_POST           => true,
                CURLOPT_POSTFIELDS     => http_build_query($post),
                CURLOPT_FAILONERROR    => false,
                CURLOPT_RETURNTRANSFER => true,
            )
        );

        $response = curl_exec($curl);
        curl_close($curl);
        $data = json_decode($response);

        var_dump("https://api.deliveo.eu/package/create?licence=" .
            $this->scopeConfig->getValue('deliveo/general/license') . "&api_key=" . $this->scopeConfig->getValue('deliveo/general/apikey'));

        if ($data !== null && $data->type != "error") {
            $order->setdeliveo_code($data->data[0]);

            $orderState = $this->scopeConfig->getValue('deliveo/general/status');
            $order->setState($orderState)->setStatus($orderState);

            $order->save();
            $packages = $data->data[0];
        } else {
            $message = '';
            if (is_string($response)) {
                $message = $response;
            }

            if ($data !== null) {
                $message = $data->msg;
            }

            return $this->messageManager->addErrorMessage(__('Sikertelen csomagfeladás, rendelésazonosító: ') . $order->getIncrementId() . __(", HIBA: ") . $message);
        }
        if (isset($packages)) {
            if (is_array($packages)) {
                $trackingNumbers = implode(", ", $packages);
            } else {
                $trackingNumbers = $packages;
            }
            $this->messageManager->addSuccessMessage(__('Sikeres csomagfeladás, csoportazonosító: ' . $trackingNumbers, 1));
            return $packages;
        } else {
            return null;
        }
    }
}
