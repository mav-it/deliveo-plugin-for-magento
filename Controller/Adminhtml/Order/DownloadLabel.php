<?php

namespace Mavit\Deliveo\Controller\Adminhtml\Order;

use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Action\Action;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Result\Page;

class DownloadLabel extends Action
{
    private $scopeConfig;
    private $resultRawFactory;
    private $fileFactory;

    public function __construct(Context $context) {
        $manager = \Magento\Framework\App\ObjectManager::getInstance();
        $this->scopeConfig           = $manager->create('Magento\Framework\App\Config\ScopeConfigInterface');
        $this->resultRawFactory      = $manager->create('Magento\Framework\Controller\Result\RawFactory');
        $this->fileFactory           = $manager->create('Magento\Framework\App\Response\Http\FileFactory');
        parent::__construct($context);
    }

    public function execute()
    {
        $apikey = $this->scopeConfig->getValue('deliveo/general/apikey');
        $licence = $this->scopeConfig->getValue('deliveo/general/license');
        $groupid = $this->getRequest()->getParam('group_id');

        $curl = curl_init();
        $url = 'https://api.deliveo.eu/label/' . $groupid . '?licence=' . $licence . '&api_key=' . $apikey;

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/pdf'
            )
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        $filename = $groupid . '_label.pdf';

        return $this->fileFactory->create(
            $filename,
            $response
        );
    }
}
