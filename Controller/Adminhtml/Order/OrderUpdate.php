<?php

namespace Mavit\Deliveo\Controller\Adminhtml\Order;

use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Action\Action;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Result\Page;

class OrderUpdate extends Action
{
    private $scopeConfig;
    private $manager;

    public function __construct(Context $context) {
        $this->manager = \Magento\Framework\App\ObjectManager::getInstance();
        $this->scopeConfig           = $this->manager->create('Magento\Framework\App\Config\ScopeConfigInterface');
        parent::__construct($context);
    }

    public function execute()
    {
        $request = $this->manager->get('Magento\Framework\App\Request\Http');
        $order = $this->_objectManager->create('Magento\Sales\Model\Order')->load($request->getParam('entity_id'));

        if (array_key_exists('packaging_unit', $request->getParams())) {
            $order->setDeliveoPackagingUnit(
                $request->getParam('packaging_unit')
            );
        }
        if (array_key_exists('delivery_mode', $request->getParams())) {
            $order->setDeliveoDeliveryMode(
                $request->getParam('delivery_mode')
            );
        }
        $order->save();
    }
}