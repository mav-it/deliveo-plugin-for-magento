<?php
namespace Mavit\Deliveo\Block\Adminhtml\OrderEdit\Tab;

/**
 * Order custom tab
 *
 */
class View extends \Magento\Backend\Block\Template implements \Magento\Backend\Block\Widget\Tab\TabInterface
{
    protected $_template = 'tab/view/my_order_info.phtml';

    /**
     * View constructor.
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\HTTP\Client\Curl $curl,
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        array $data = [],
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    ) {
        $this->curl = $curl;
        $this->scopeConfig = $scopeConfig;
        $this->_coreRegistry = $registry;
        parent::__construct($context, $data);
    }

    /**
     * Retrieve order model instance
     *
     * @return \Magento\Sales\Model\Order
     */
    public function getOrder()
    {
        return $this->_coreRegistry->registry('current_order');
    }
    /**
     * Retrieve order model instance
     *
     * @return \Magento\Sales\Model\Order
     */
    public function getOrderId()
    {
        return $this->getOrder()->getEntityId();
    }

    /**
     * Retrieve order increment id
     *
     * @return string
     */
    public function getOrderIncrementId()
    {
        return $this->getOrder()->getIncrementId();
    }
    /**
     * {@inheritdoc}
     */
    public function getTabLabel()
    {
        return __('Deliveo');
    }

    /**
     * {@inheritdoc}
     */
    public function getTabTitle()
    {
        return __('Deliveo');
    }

    /**
     * {@inheritdoc}
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function isHidden()
    {
        return false;
    }

    public function getDeliveoCode()
    {
        return $this->getOrder()->getDeliveoCode() ?? null;
    }

    public function getSignature()
    {
        $apikey = $this->scopeConfig->getValue('deliveo/general/apikey');
        $licence = $this->scopeConfig->getValue('deliveo/general/license');
        $package = $this->getOrder()->getDeliveoCode();
        
        $url = "https://api.deliveo.eu/signature/$package?licence=$licence&api_key=$apikey&lang=HU";
        $this->curl->get($url);

        $response = json_decode($this->curl->getBody());
        if ($response->type == "success")
        {
            return $response;
        } else {
            return null;
        }
    }

    public function getPackageInfo()
    {
        $apikey = $this->scopeConfig->getValue('deliveo/general/apikey');
        $licence = $this->scopeConfig->getValue('deliveo/general/license');
        $package = $this->getOrder()->getDeliveoCode();
        
        $url = "https://api.deliveo.eu/package_log/$package?licence=$licence&api_key=$apikey&lang=HU";
        $this->curl->get($url);

        $response = json_decode($this->curl->getBody());
        if ($response->type == "success")
        {
            return $response->data;
        } else {
            return null;
        }
    }

    public function getDeliveoDetails() {
        $apikey = $this->scopeConfig->getValue('deliveo/general/apikey');
        $licence = $this->scopeConfig->getValue('deliveo/general/license');
        $groupid = $this->getOrder()->getDeliveoCode();

        $url = 'https://api.deliveo.eu/package/' . $groupid . '?licence=' . $licence . '&api_key=' . $apikey.'&lang=HU';

        $this->curl->get($url);
        $response = $this->curl->getBody();

        if ($response === false) return null;
        return json_decode($response)->data;
    }

    public function downloadableSign() {
        $apikey = $this->scopeConfig->getValue('deliveo/general/apikey');
        $licence = $this->scopeConfig->getValue('deliveo/general/license');
        $groupid = $this->getOrder()->getDeliveoCode();

        $url = 'https://api.deliveo.eu/signature/' . $groupid . '?licence=' . $licence . '&api_key=' . $apikey.'&lang=HU';

        $this->curl->get($url);
        $response = $this->curl->getBody();

        if(isset(json_decode($response)->type) && json_decode($response)->type == "error"){
            return false;
        }

        $data = base64_encode($response);

        $signature = base64_decode($data);

        $downloadable = true;
        if (strpos($signature, 'signature') <= 0) {
            $downloadable = false;
        }

        return $downloadable;
    }
}
