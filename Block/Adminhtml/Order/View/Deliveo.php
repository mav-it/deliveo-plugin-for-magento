<?php
namespace Mavit\Deliveo\Block\Adminhtml\Order\View;

use \Magento\Framework\Data\Form\FormKey;

class Deliveo extends \Magento\Framework\View\Element\Template
{

    protected $formKey;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        FormKey $formKey
    ) {
        parent::__construct($context);
        $this->formKey = $formKey;
    }

    public function Deliveo()
    {

        $orderId = $this->getRequest()->getParam('order_id');
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $order = $objectManager->create('\Magento\Sales\Model\OrderRepository')->get($orderId);
        if ($order->getDeliveoCode() == "" || $order->getDeliveoCode() == null) {
            return "A csomag nincs feladva";
        } else {
            return $order->getDeliveoCode();
            // return $this->DeliveoState();
        }
    }
    public function DeliveoState()
    {
        return "Winnerke";
    }

    public function getFormKey()
    {
        return $this->formKey->getFormKey();
    }
}
