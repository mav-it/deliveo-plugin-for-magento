<?php

namespace Winnerke\Deliveo\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

/**
 * @codeCoverageIgnore
 */
class InstallSchema implements InstallSchemaInterface
{
    /**
     * Upgrades DB schema for a module
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;

        $installer->startSetup();
        $connection = $installer->getConnection();

        $table = $installer->getTable('sales_order');
        $columns = [
            'deliveo_code' => [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'nullable' => true,
                'comment' => 'DeliveoID',
            ],
            'deliveo_delivery_mode' => [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'nullable' => true,
                'comment' => 'Deliveo shipment options',
            ],
            'deliveo_packaging_unit' => [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'nullable' => true,
                'comment' => 'Deliveo packaging unit',
            ],
        ];

        foreach ($columns as $name => $definition) {
            $connection->addColumn($table, $name, $definition);
        }

        $table = $installer->getTable('sales_order_grid');
        $columns = [
            'deliveo_code' => [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'nullable' => true,
                'comment' => 'DeliveoID',
            ],
            'deliveo_delivery_mode' => [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'nullable' => true,
                'comment' => 'Deliveo shipment options',
            ],
            'deliveo_packaging_unit' => [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'nullable' => true,
                'comment' => 'Deliveo packaging unit',
            ],
        ];

        foreach ($columns as $name => $definition) {
            $connection->addColumn($table, $name, $definition);
        }

        $installer->endSetup();
    }
}
