<?php

namespace Mavit\Deliveo\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;

class Data extends AbstractHelper
{
    /**
     * @var EncryptorInterface
     */
    protected $encryptor;

    /**
     * @param Context $context
     * @param EncryptorInterface $encryptor
     */
    public function __construct(
        Context $context,
        \Magento\Framework\Encryption\EncryptorInterface $encryptor
    ) {
        parent::__construct($context);
        $this->encryptor = $encryptor;
    }

    /*
     * @return string
     */
    public function getApikey($scope = ScopeConfigInterface::SCOPE_TYPE_DEFAULT)
    {
        return $this->scopeConfig->getValue(
            'deliveo/general/apikey',
            $scope
        );
    }

    /*
     * @return string
     */
    public function getLicense($scope = ScopeConfigInterface::SCOPE_TYPE_DEFAULT)
    {
        $secret = $this->scopeConfig->getValue(
            'deliveo/general/license',
            $scope
        );
        // $secret = $this->encryptor->decrypt($secret);

        return $secret;
    }

    /*
     * @return string
     */
    public function getOption($scope = ScopeConfigInterface::SCOPE_TYPE_DEFAULT)
    {
        return $this->scopeConfig->getValue(
            'deliveo/general/option',
            $scope
        );
    }

    /*
     * @return int
     */
    public function getHeight($scope = ScopeConfigInterface::SCOPE_TYPE_DEFAULT)
    {
        return $this->scopeConfig->getValue(
            'deliveo/general/height',
            $scope
        );
    }

    /*
     * @return int
     */
    public function getWidth($scope = ScopeConfigInterface::SCOPE_TYPE_DEFAULT)
    {
        return $this->scopeConfig->getValue(
            'deliveo/general/width',
            $scope
        );
    }

    /*
     * @return string
     */
    public function getDepth($scope = ScopeConfigInterface::SCOPE_TYPE_DEFAULT)
    {
        return $this->scopeConfig->getValue(
            'deliveo/general/depth',
            $scope
        );
    }

    /*
     * @return string
     */
    public function getSname($scope = ScopeConfigInterface::SCOPE_TYPE_DEFAULT)
    {
        return $this->scopeConfig->getValue(
            'deliveo/general/sname',
            $scope
        );
    }

    /*
     * @return string
     */
    public function getScountry($scope = ScopeConfigInterface::SCOPE_TYPE_DEFAULT)
    {
        return $this->scopeConfig->getValue(
            'deliveo/general/scountry',
            $scope
        );
    }

    /*
     * @return string
     */
    public function getScity($scope = ScopeConfigInterface::SCOPE_TYPE_DEFAULT)
    {
        return $this->scopeConfig->getValue(
            'deliveo/general/scity',
            $scope
        );
    }

    /*
     * @return string
     */
    public function getSpostcode($scope = ScopeConfigInterface::SCOPE_TYPE_DEFAULT)
    {
        return $this->scopeConfig->getValue(
            'deliveo/general/postcode',
            $scope
        );
    }

    /*
     * @return string
     */
    public function getAddress1($scope = ScopeConfigInterface::SCOPE_TYPE_DEFAULT)
    {
        return $this->scopeConfig->getValue(
            'deliveo/general/address1',
            $scope
        );
    }

    /*
     * @return string
     */
    public function getAddress2($scope = ScopeConfigInterface::SCOPE_TYPE_DEFAULT)
    {
        return $this->scopeConfig->getValue(
            'deliveo/general/address2',
            $scope
        );
    }

    /*
     * @return string
     */
    public function getPhone($scope = ScopeConfigInterface::SCOPE_TYPE_DEFAULT)
    {
        return $this->scopeConfig->getValue(
            'deliveo/general/phone',
            $scope
        );
    }

    /*
     * @return string
     */
    public function getEmail($scope = ScopeConfigInterface::SCOPE_TYPE_DEFAULT)
    {
        return $this->scopeConfig->getValue(
            'deliveo/general/email',
            $scope
        );
    }

    /*
     * @return bool
     */
    public function isPriority($scope = ScopeConfigInterface::SCOPE_TYPE_DEFAULT)
    {
        return $this->scopeConfig->isSetFlag(
            'deliveo/general/priority',
            $scope
        );
    }

    /*
     * @return bool
     */
    public function isSaturday($scope = ScopeConfigInterface::SCOPE_TYPE_DEFAULT)
    {
        return $this->scopeConfig->isSetFlag(
            'deliveo/general/saturday',
            $scope
        );
    }

    /*
     * @return bool
     */
    public function isInsurance($scope = ScopeConfigInterface::SCOPE_TYPE_DEFAULT)
    {
        return $this->scopeConfig->isSetFlag(
            'deliveo/general/insurance',
            $scope
        );
    }

    /*
     * @return bool
     */
    public function isOrderref($scope = ScopeConfigInterface::SCOPE_TYPE_DEFAULT)
    {
        return $this->scopeConfig->isSetFlag(
            'deliveo/general/orderref',
            $scope
        );
    }

    /*
     * @return string
     */
    public function getFreight($scope = ScopeConfigInterface::SCOPE_TYPE_DEFAULT)
    {
        return $this->scopeConfig->getValue(
            'deliveo/general/freight',
            $scope
        );
    }
}
