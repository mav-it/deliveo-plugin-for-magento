<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Mavit\Deliveo\Ui\Component\MassAction\Group;

use Magento\Customer\Model\ResourceModel\Group\CollectionFactory;
use Magento\Framework\UrlInterface;
use Zend\Stdlib\JsonSerializable;

/**
 * Class ShippingOptions
 */
class ShippingOptions extends \Magento\Ui\Component\MassAction implements JsonSerializable
{
    /**
     * @var array
     */
    protected $options;

    /**
     * @var CollectionFactory
     */
    protected $collectionFactory;

    /**
     * Additional options params
     *
     * @var array
     */
    protected $data;

    /**
     * @var UrlInterface
     */
    protected $urlBuilder;

    /**
     * Base URL for subactions
     *
     * @var string
     */
    protected $urlPath;

    /**
     * Param name for subactions
     *
     * @var string
     */
    protected $paramName;

    /**
     * Additional params for subactions
     *
     * @var array
     */
    protected $additionalData = [];

    /**
     * Constructor
     *
     * @param CollectionFactory $collectionFactory
     * @param UrlInterface $urlBuilder
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Message\ManagerInterface $messageManager,
        CollectionFactory $collectionFactory,
        UrlInterface $urlBuilder,
        array $data = [],
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    ) {
        $this->collectionFactory = $collectionFactory;
        $this->data              = $data;
        $this->scopeConfig       = $scopeConfig;
        $this->urlBuilder        = $urlBuilder;
        $this->messageManager    = $messageManager;
    }

    /**
     * Get action options
     *
     * @return array
     */
    public function jsonSerialize()
    {
        $items = [];
        if ($this->options === null) {
            $apikey  = $this->scopeConfig->getValue('deliveo/general/apikey', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
            $licence = $this->scopeConfig->getValue('deliveo/general/license', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
            $curl    = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL            => "https://api.deliveo.eu/delivery?licence=$licence&api_key=$apikey",
                CURLOPT_FAILONERROR    => false,
                CURLOPT_RETURNTRANSFER => true,
            ));

            $response = json_decode(curl_exec($curl));

//         echo "<pre>";
            //         var_dump($response->type);
            //         echo "</pre>";

            curl_close($curl);
            // die;
            $this->messageManager->addErrorMessage(__('Nem sikerült lekérdezni a szállítási módokat.') . __(" HIBA: ") . $response->msg, 1);

            if ($response->type == "error") {
                $options = $this->collectionFactory->create()->setRealGroupsFilter()->toOptionArray();
                $this->prepareData();

                $items[] = array(
                    'value' => "99",
                    'label' => "Hiba: " . $response->msg,
                );
                // $resultRedirect = $this->resultRedirectFactory->create();
                // return $resultRedirect->setPath($this->getComponentRefererUrl());
                foreach ($items as $optionCode) {
                    $this->options[$optionCode['value']] = [
                        'type'  => $optionCode['value'],
                        'label' => $optionCode['label'],
                    ];

                    if ($this->urlPath && $this->paramName) {
                        $this->options[$optionCode['value']]['url'] = $this->urlBuilder->getUrl(
                            $this->urlPath,
                            [$this->paramName => $optionCode['value']]
                        );
                    }

                    $this->options[$optionCode['value']] = array_merge_recursive(
                        $this->options[$optionCode['value']],
                        $this->additionalData
                    );
                }

                $this->options = array_values($this->options);
            } else {
                $array = $response->data;

                if ($this->options === null) {
                    $options = $this->collectionFactory->create()->setRealGroupsFilter()->toOptionArray();
                    $this->prepareData();
                    foreach ($array as $item) {
                        $items[] = array(
                            'value' => $item->value,
                            'label' => $item->description,
                        );
                    }

                    foreach ($items as $optionCode) {
                        $this->options[$optionCode['value']] = [
                            'type'  => $optionCode['value'],
                            'label' => $optionCode['label'],
                        ];

                        if ($this->urlPath && $this->paramName) {
                            $this->options[$optionCode['value']]['url'] = $this->urlBuilder->getUrl(
                                $this->urlPath,
                                [$this->paramName => $optionCode['value']]
                            );
                        }

                        $this->options[$optionCode['value']] = array_merge_recursive(
                            $this->options[$optionCode['value']],
                            $this->additionalData
                        );
                    }

                    $this->options = array_values($this->options);
                }
            }

            return $this->options;
        }
    }

    /**
     * Prepare addition data for subactions
     *
     * @return void
     */
    protected function prepareData()
    {
        foreach ($this->data as $key => $value) {
            switch ($key) {
                case 'urlPath':
                    $this->urlPath = $value;
                    break;
                case 'paramName':
                    $this->paramName = $value;
                    break;
                default:
                    $this->additionalData[$key] = $value;
                    break;
            }
        }
    }
}
