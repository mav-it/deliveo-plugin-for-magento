<?php

namespace Mavit\Deliveo\Ui\Component\Columns;

use \Magento\Framework\Api\SearchCriteriaBuilder;
use \Magento\Framework\View\Element\UiComponentFactory;
use \Magento\Framework\View\Element\UiComponent\ContextInterface;
use \Magento\Sales\Api\OrderRepositoryInterface;
use \Magento\Ui\Component\Listing\Columns\Column;

class PackagingUnit extends Column
{
    protected $_orderRepository;
    protected $_searchCriteria;

    public function __construct(ContextInterface $context, UiComponentFactory $uiComponentFactory, OrderRepositoryInterface $orderRepository, SearchCriteriaBuilder $criteria, array $components = [], array $data = [])
    {
        $this->_orderRepository = $orderRepository;
        $this->_searchCriteria = $criteria;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    public function prepareDataSource(array $dataSource)
    {
        $manager = \Magento\Framework\App\ObjectManager::getInstance();
        $coreConfig = $manager->create('Mavit\Deliveo\Model\Config\Source\CoreConfig');
        $defaultPackagingUnit = $coreConfig->getConfig('deliveo/general/packaging_unit');

        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as &$item) {
                $order = $this->_orderRepository->get($item["entity_id"]);
                $maxValue = $order->getData("total_item_count");

                $disabled = $defaultPackagingUnit != '3';
                $value = '1';

                if ($defaultPackagingUnit == '2') {
                    $value = $maxValue;
                }
                if ($defaultPackagingUnit=='3' && $order->getPackagingUnit() != null) {
                    $value = $order->getPackagingUnit();
                }

                $order = $this->_orderRepository->get($item["entity_id"]);
                $status = $order->getData("deliveo_code");

                $item[$this->getData('name')] = is_null($status) ?
                    '<input id="packaging_unit_'.$item["entity_id"].'" style="width:100%" onchange="jQuery.ajax({url: \'/admin/deliveomassactions/order/OrderUpdate\', data: {entity_id: \''.$item["entity_id"].'\', packaging_unit: jQuery(\'#packaging_unit_'.$item["entity_id"].'\').val()}});" class="admin__control-text" max-length=3 '.($disabled ? 'disabled' : '').' name="packaging_unit" type="number" step="1" min="1" max="'.$maxValue.'" value="'.$value.'">'
                    : '';
            }
        }
        return $dataSource;
    }

}