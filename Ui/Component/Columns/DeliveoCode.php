<?php

namespace Mavit\Deliveo\Ui\Component\Columns;

use \Magento\Framework\Api\SearchCriteriaBuilder;
use \Magento\Framework\View\Element\UiComponentFactory;
use \Magento\Framework\View\Element\UiComponent\ContextInterface;
use \Magento\Sales\Api\OrderRepositoryInterface;
use \Magento\Ui\Component\Listing\Columns\Column;

class DeliveoCode extends Column
{
    protected $_orderRepository;
    protected $_searchCriteria;

    public function __construct(ContextInterface $context, UiComponentFactory $uiComponentFactory, OrderRepositoryInterface $orderRepository, SearchCriteriaBuilder $criteria, array $components = [], array $data = [])
    {
        $this->_orderRepository = $orderRepository;
        $this->_searchCriteria = $criteria;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as &$item) {

                $order = $this->_orderRepository->get($item["entity_id"]);
                $status = $order->getData("deliveo_code");

                $item[$this->getData('name')] = $status ?? $this->getShippingOptionsSelect($item, $order->getDeliveryMode());
            }
        }

        return $dataSource;
    }

    private function getShippingOptionsSelect(array &$item, $deliveryMode)
    {
        $manager = \Magento\Framework\App\ObjectManager::getInstance();
        $obj = $manager->create('Mavit\Deliveo\Model\Config\Source\Delivery');
        $options = $obj->toOptionArray();

        if ($options == null) {
            $manager = \Magento\Framework\App\ObjectManager::getInstance();
            $urlBuilder = $manager->create('Magento\Framework\UrlInterface');
            return '<a href="'.$urlBuilder->getUrl('deliveomassactions/configuration/index').'">Deliveo konfiguráció</a>';
        }

        $coreConfig = $manager->create('Mavit\Deliveo\Model\Config\Source\CoreConfig');
        $defaultDelivery = $coreConfig->getConfig('deliveo/general/defaultdeliverymode');

        if ($deliveryMode != null) {
            $defaultDelivery = $deliveryMode;
        }

        $inputString = '<select onchange="jQuery.ajax({url: \'/admin/deliveomassactions/order/OrderUpdate\', data: {entity_id: \''.$item["entity_id"].'\', delivery_mode: jQuery(\'#delivery_mode_'.$item["entity_id"].'\').val()}});" id="delivery_mode_'.$item["entity_id"].'" class="admin__control-select">';
        foreach ($options as $option) {
            $inputString .= '<option '. ($option['value']==$defaultDelivery ? 'selected' : '') .' value="'.$option['value'].'">'.$option['label'].'</option>';
        }
        $inputString .= '</select>';
        return $inputString;
    }

}