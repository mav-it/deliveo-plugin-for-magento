<?php

namespace Mavit\Deliveo\Model\Config\Source;

use Magento\Framework\Option\ArrayInterface;
use \Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Config\Storage\WriterInterface;

class CoreConfig
{
    /**
     *  @var WriterInterface
     */
    protected $configWriter;

    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        WriterInterface $configWriter
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->configWriter = $configWriter;
    }

    public function getConfig($configPath) {
        return $this->scopeConfig->getValue($configPath);
    }

    public function setConfig($configPath, $value) {
        $this->configWriter->save($configPath, $value);
    }
}