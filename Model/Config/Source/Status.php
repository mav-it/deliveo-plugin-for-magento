<?php

namespace Mavit\Deliveo\Model\Config\Source;

use Magento\Framework\Option\ArrayInterface;
Use Magento\Sales\Model\ResourceModel\Order\Status\Collection;


class Status implements ArrayInterface
{
    public function __construct(
        \Magento\Sales\Model\ResourceModel\Order\Status\Collection $statusCollection
    )
    {
        $this->statusCollection = $statusCollection;
        
    }
    public function toOptionArray()
    {
        return [
            [
                'value' => 'teszt1',
                'label' => 'TESZT1'
            ],
            [
                'value' => 'teszt2',
                'label' => 'TESZT2'
            ]
        ];
    }
        
    
}