<?php


namespace Mavit\Deliveo\Model\Config\Source;

use Magento\Framework\Option\ArrayInterface;
use \Magento\Framework\App\Config\ScopeConfigInterface;

class Delivery implements ArrayInterface
{

    private $scopeConfig;
    private $coreConfig;

    public function __construct(
        ScopeConfigInterface $scopeConfig
    ) {
        $this->scopeConfig = $scopeConfig;
        $manager = \Magento\Framework\App\ObjectManager::getInstance();
        $this->coreConfig = $manager->create('Mavit\Deliveo\Model\Config\Source\CoreConfig');
    }

    public function toOptionArray()
    {
        $deliveryModes = $this->coreConfig->getConfig('deliveo/general/defaultdeliverymodes');
        if ($deliveryModes == null) {
            $deliveryModes = $this->updateConfig();
            if ($deliveryModes == null) {
                return null;
            }
        }
        return json_decode($deliveryModes, true);
    }

    public function updateConfig($apiKey = null, $licence = null)
    {
        if ($apiKey == null && $licence == null) {
            $apiKey = $this->scopeConfig->getValue('deliveo/general/apikey');
            $licence = $this->scopeConfig->getValue('deliveo/general/license');
        }

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, "https://api.deliveo.eu/delivery?licence=$licence&api_key=$apiKey");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);

        $response = curl_exec($curl);
        curl_close($curl);
        $array = json_decode($response);

        if ($array !== null && isset($array->data)) {
            $array = $array->data;

            foreach ($array as $element) {
                $options[] = array(
                    'value' => $element->value,
                    'label' => $element->description,
                );
            }
            $this->coreConfig->setConfig('deliveo/general/defaultdeliverymodes', json_encode($options));
            return json_encode($options);
        }
        return null;
    }
}
