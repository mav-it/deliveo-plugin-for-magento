<?php

namespace Mavit\Deliveo\Model\Config\Source;

use Magento\Framework\Option\ArrayInterface;

class Option implements ArrayInterface
{
    public function __construct(\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig)
    {
        $this->scopeConfig = $scopeConfig;
    }
    public function toOptionArray()
    {
        $apikey = $this->scopeConfig->getValue('deliveo/general/apikey', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $licence = $this->scopeConfig->getValue('deliveo/general/license', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.deliveo.eu/delivery?licence=$licence&api_key=$apikey",
            CURLOPT_FAILONERROR => false,
            CURLOPT_RETURNTRANSFER => true,
        ));

        $response = curl_exec($curl);
        curl_close($curl);
        $array = json_decode($response);

        if ($array->error_code == 0) {
            $array = $array->data;

            foreach ($array as $element) {
                $options[] = array(
                    'value' => $element->value,
                    'label' => $element->description,
                );
            }
            return $options;
        } else {
            return [
                ['value' => '99',
                    'label' => 'Nem sikerült lekérdezni a szállítási opciókat'],
            ];
        }

    }
}
