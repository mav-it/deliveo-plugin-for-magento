<?php

namespace Mavit\Deliveo\Model\Config\Source;

use Magento\Framework\App\Config\Storage\WriterInterface;
use Magento\Framework\Option\ArrayInterface;
use \Magento\Framework\App\Config\ScopeConfigInterface;

class Freight implements ArrayInterface
{
    /**
     *  @var WriterInterface
     */
    protected $configWriter;

    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        WriterInterface $configWriter
    ) {
        $this->scopeConfig  = $scopeConfig;
        $this->configWriter = $configWriter;
    }

    public function toOptionArray()
    {
        $apikey  = $this->scopeConfig->getValue('deliveo/general/apikey', \Magento\Store\Model\ScopeInterface::SCOPE_STORE, 0);
        $licence = $this->scopeConfig->getValue('deliveo/general/license', \Magento\Store\Model\ScopeInterface::SCOPE_STORE, 0);

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, "https://api.deliveo.eu/freight?licence=$licence&api_key=$apikey");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $response = curl_exec($curl);
        curl_close($curl);
        $array = json_decode($response);
        if ($array !== null) {
            $array = $array->data;

            foreach ($array as $element) {
                $options[] = array(
                    'value' => $element->value,
                    'label' => $element->description,
                );
            }
            return $options;
        } else {
            return [
                ['value' => '99',
                    'label'  => 'Nem sikerült lekérdezni a fuvarköltséget!'],
            ];
        }

    }
}
