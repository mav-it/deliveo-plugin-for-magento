<?php
namespace Mavit\Deliveo\Model;

use Magento\Framework\Model\AbstractModel;

    class Data extends AbstractModel
    {   
        protected function _construct()
        {
            $this->_init('Mavit\Deliveo\Model\ResourceModel\Data');
        }
    }