<?php 
namespace Mavit\Deliveo\Model\ResourceModel\Data;


use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;


class Collection extends AbstractCollection
{
    protected function _construct()
    {
        $this->_init(
        'Mavit\Deliveo\Model\Data',
        'Mavit\Deliveo\Model\ResourceModel\Data'
    );
    }
}