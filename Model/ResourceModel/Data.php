<?php

namespace Mavit\Deliveo\Model\ResourceModel;


use \Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Data extends AbstractDb
{
    protected function _construct()
    {
        $this->_init('deliveo_settings', 'shop_id'); 
    }
}