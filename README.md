# Deliveo plugin Magento webáruházhoz

A plugin lehetővé teszi, hogy a webshop megrendelések táblájából közvetlenül feladjuk a küldeményeket a Deliveo-ba, majd kövessük státuszokat a naplóban, és csomagcímkét, aláíráslapot nyomtassunk hozzá.

A plugin telepítéséhez a webshop tárhelyéhez parancssori módon hozzá kell férnünk. A telepítés 5 lépésben megy végbe. A parancsor használatához haladó számítógépes ismeretekre lesz szükség.

1. Lépjünk be a tárhelyfiókba, és parancsorban navigáljunk a Magento telepítésünk gyökérkönyvtárába. (cPanel alapú szervereknél ez gyakran a "public_html" könyvtár lesz.) Az aktuális munkakönyvtárat a `pwd` paranccsal ellenőrizhetjük.
2. Itt adjuk ki következő parancsot: `mkdir -p app/code/Mavit/Deliveo && wget -qO- https://gitlab.com/mav-it/deliveo-plugin-for-magento/-/archive/master/deliveo-plugin-for-magento-master.tar.gz -O - | tar xvz -C app/code/Mavit/Deliveo --strip-components=1 --overwrite`
3. Ha a 2. pont nem adott hibát, akkor adjuk ki a következő parancsot: `php bin/magento module:status Mavit_Deliveo`, itt ezt kell visszakapnunk: `Mavit_Deliveo :  Module is disabled`. Ezzel a paranccsal csak ellenőriztük, hogy a plugin készen áll-e a telepítésre.
4. Ha a 3. pontban az elvárt eredményt kaptuk, akkor adjuk ki a következő parancsot: `php bin/magento setup:upgrade` Ezzel a plugint telepítettük.
5. Utolsó lépésként a cache kiürítéséhez adjuk ki a következő parancsot: ` php bin/magento cache:flush`

A plugin telepítése ezzel befejeződött, használatba vehető. A plugint Magento 2.4.1 verzión teszteltük.
